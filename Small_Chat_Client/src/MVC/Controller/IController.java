package MVC.Controller;

import Observer.IObserver;
import Observer.Observable;

public interface IController {
	public void setModel(Observable model);
	
	public void setView(IObserver view);
}
