package Observer;

import java.util.Collection;
import java.util.HashSet;

public class Observable implements IObservable {

	private Collection<IObserver> observers = new HashSet<IObserver>();

	public void addObserver(IObserver observer) {
		if (observer == null)
			throw new NullPointerException("Null pointer");
		if (observers.contains(observer))
			throw new IllegalArgumentException("This observer has alredy added"
					+ observer);
		observers.add(observer);
		notifyObserver(observer);

	}

	public void removeObserver(IObserver observer) {
		if (observer == null)
			throw new NullPointerException("Null pointer");
		if (!observers.contains(observer))
			throw new IllegalArgumentException("Unknown observer"
					+ observer);
		observers.remove(observer);
	}

	public void notifyObserver(IObserver observer) {
		assert observer != null;
		observer.update(this);
	}

	public void notifyObservers() {
		for (final IObserver observer : observers)
			notifyObserver(observer);
	}

}
