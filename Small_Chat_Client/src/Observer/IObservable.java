package Observer;

public interface IObservable {
	public void addObserver(IObserver observer);

	public void removeObserver(IObserver observer);

	public void notifyObserver(IObserver observer);

	public void notifyObservers();
}
