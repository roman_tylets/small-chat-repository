package WorkWithTextFields;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

/**
 * class SDocumentTextLimit - limits length of the entered in text field string
 * @author Dreamer
 * {@value MaxLength - max length of the entered in text field string}
 */
public class SDocumentTextLimit extends PlainDocument {
	 
    protected int MaxLength;
 
    public SDocumentTextLimit(int MaxLength) {
        super();
        this.MaxLength = MaxLength;
    }
 
    public SDocumentTextLimit(int MaxLength, DocumentFilter df) {
        super();
        this.MaxLength = MaxLength;
        setDocumentFilter(df);
    }
    @Override
    public void insertString(int offset, String str, AttributeSet attr)
            throws BadLocationException {
        if (str != null && (str.length() + offset) <= MaxLength) {
            super.insertString(offset, str, attr);
        }
    }
    @Override
    public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        if (text != null && (text.length() + offset) <= MaxLength) {
            super.replace(offset, length, text, attrs);
        }
    }
}
