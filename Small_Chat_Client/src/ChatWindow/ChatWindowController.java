package ChatWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import MVC.Controller.IController;
import Networking.Client;
import Observer.IObserver;
import Observer.Observable;

public class ChatWindowController implements IController, ActionListener,
		KeyListener, WindowListener {
	private ChatWindowModel model;
	private ChatWindowView view;
	private Client client;
	
	public ChatWindowController(Client client) {
		System.out.println("Controler constructed");
		this.client=client;
		
		ReadMessageFromServerThread reader = new ReadMessageFromServerThread();
		reader.start();
	}

	/** Listeners of View **/
	/*** ACTION LISTENER ***/
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == view.getSendButton()) {
			sendMessage();
			
		}

	}
	/*** ACTION LISTENER ***/
	
	/*** KEY LISTENER ***/
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER && e.isShiftDown()) {
			if (e.getSource() == view.getEnterMessage()) {
				sendMessage();
			}
		}
			else if (e.getKeyCode() == KeyEvent.VK_ENTER)
			{
			if (e.getSource() == view.getSendButton()) {
				sendMessage();
				
			}
		}

	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent e) {

		

	}
	/*** KEY LISTENER ***/
	
	/*** WINDOW LISTENER***/
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosed(WindowEvent arg0) {
		System.out.println("Chat window: closed");
	}

	public void windowClosing(WindowEvent arg0) {
		System.out.println("Chat window: closing");
		client.disconnectFromServer();
	}

	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	/*** WINDOW LISTENER***/
	/** Listeners of View **/

	public void sendMessage()
	{
		if(!view.getEnterMessage().getText().equals(""))
		{
			client.sendMessage(view.getEnterMessage().getText());
			view.getEnterMessage().setText("");
		}
		
	}
	
	
	public void setModel(Observable model) {
		this.model = (ChatWindowModel) model;
	}

	public void setView(IObserver view) {
		this.view = (ChatWindowView) view;
	}

	public void setMessage(String message) {
		
		String dt=new java.text.SimpleDateFormat("kk:mm:ss").format(java.util.Calendar.getInstance().getTime());
		String textMessage;
		textMessage="["+dt+"] From "+message;

		
		model.settextMessage(textMessage);
	}
	
	public void setUserName (String name)
	{
		model.setusersInChat(name);
	}
	
	public void removeUserName (String name)
	{
		model.removeusersInChat(name);
	}

	class ReadMessageFromServerThread extends Thread {

		  
		  public void run() {
		  
			  String message=null;	  
			  
		   while (!Thread.interrupted()) {
			   			
			   message=client.reciveMessage();
			   
			   Pattern patternIsMessageUserName = Pattern.compile("USER_NAME_(.*)");
			   Matcher matcherIsMessageUserName = patternIsMessageUserName.matcher(message);
			   boolean isMessageUserName = matcherIsMessageUserName.matches();
			    
			   Pattern patternIsMessageQuitUserName = Pattern.compile("QUIT_USER_NAME_(.*)");
			   Matcher matcherIsMessageQuitUserName = patternIsMessageQuitUserName.matcher(message);
			   boolean isMessageQuitUserName = matcherIsMessageQuitUserName.matches();
			   
			   System.out.println(isMessageUserName);
			   
			   if(isMessageUserName==true)
			   {
			   setUserName(matcherIsMessageUserName.group(1));
			   }
			   else if(isMessageQuitUserName==true)
			   {
			   removeUserName(matcherIsMessageQuitUserName.group(1));
			   }
			   else
			   {
			   setMessage(message);
			   }
		   
		    }
		   
		  }

		 }

}



	

