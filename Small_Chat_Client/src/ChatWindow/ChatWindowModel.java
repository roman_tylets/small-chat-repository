package ChatWindow;

import java.util.HashSet;
import java.util.Set;

import Observer.Observable;
/**
 * 
 * @author Dreamer
 *
 */
public class ChatWindowModel extends Observable
{
	private String textMessage;//Message that view in main chat
	private Set <String> usersInChat;//List of users that are in chat now
	
	
	public ChatWindowModel(){

		String dt=new java.text.SimpleDateFormat("kk:mm:ss").format(java.util.Calendar.getInstance().getTime());
		
		usersInChat=new HashSet<String>();
		
		textMessage="["+dt+"] "+"From SmallChat: "+"Welcome to small chat!";
	}
	
	public String gettextMessage()
	{
		return textMessage;
	}
	
	public void settextMessage(String textMessage)
	{
		if (textMessage == null)
			throw new NullPointerException("Null parametr");
		this.textMessage = textMessage;
		notifyObservers();
	}
	
	
	public HashSet<String> getusersInChat()
	{
		return (HashSet<String>) usersInChat;
	}
	
	public void setusersInChat(String userInChat)
	{
		if (usersInChat == null)
			throw new NullPointerException("Null parametr");
		this.usersInChat.add(userInChat);
		notifyObservers();
	}
	
	public void removeusersInChat(String userInChat)
	{
		if (usersInChat == null)
			throw new NullPointerException("Null parametr");
		this.usersInChat.remove(userInChat);
		notifyObservers();
	}
	
	
}
