package ChatWindow;

import ChatWindow.ChatWindowController;
import ChatWindow.ChatWindowModel;
import ChatWindow.ChatWindowView;
import Networking.Client;

public class StartChatWindow {

	
	public StartChatWindow(Client client)
	{
		/***CHATWINDOWMODEL***/
	ChatWindowModel chatWindowModel=new ChatWindowModel();
	ChatWindowView chatWindowView=new ChatWindowView();
	
	chatWindowModel.addObserver(chatWindowView);
			
	ChatWindowController chatWindowController=new ChatWindowController(client);
	
	chatWindowController.setModel(chatWindowModel);
	chatWindowController.setView(chatWindowView);
	
	chatWindowView.addController(chatWindowController);
	
	
	
	
	/***CHATWINDOWMODEL***/
	}
	
}
