package ChatWindow;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import MVC.Controller.IController;
import Observer.IObserver;
import Observer.Observable;

public class ChatWindowView extends JFrame implements IObserver 
{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -8066343142933311677L;
	
	private JFrame MainChatFrame = new JFrame("Small Chat");
	private JTextArea mainMessages = new JTextArea();
	private JTextArea userList = new JTextArea();	
	private JTextArea enterMessage = new JTextArea();
	private JButton sendButton = new JButton("Send");
	
	
	public ChatWindowView(){
		
		System.out.println("ChatWindowView constructed");
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		userList.setToolTipText("Users online");
		sendButton.setToolTipText("Click this button to send the message");
		enterMessage.setToolTipText("Enter message in this field. U can press Shift+Enter to send.");
		
		/**MainChatFrame**/
		
		MainChatFrame.setDefaultCloseOperation( EXIT_ON_CLOSE); 
		
		MainChatFrame.setSize(new Dimension(screenSize.width/2,screenSize.height/3));
		
		MainChatFrame.setMinimumSize(new Dimension(screenSize.width/4,screenSize.height/4));
		MainChatFrame.setPreferredSize(new Dimension(screenSize.width/2,screenSize.height/3));
		MainChatFrame.setMaximumSize(screenSize);
		/**MainChatFrame**/
		
		
		/**mainMessages**/
		
					
		mainMessages.setEditable(false);
		
		DefaultCaret mainMessagesCaret = (DefaultCaret)mainMessages.getCaret();
		mainMessagesCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane mainMessagesScroll = new JScrollPane();
		mainMessagesScroll.setViewportView(mainMessages);
		
		setPreferredSize(new Dimension(screenSize.width*7/(2*10),screenSize.height/3));
		mainMessagesScroll.setMaximumSize(new Dimension(screenSize.width*10/13,screenSize.height));
		/**mainMessages**/
		
		/**userList**/
			
		userList.setEditable(false);
		
		DefaultCaret userListCaret = (DefaultCaret)userList.getCaret();
		userListCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane userListScroll = new JScrollPane();
		userListScroll.setViewportView(userList);
		
		userListScroll.setPreferredSize(new Dimension(screenSize.width*3/(6*10),screenSize.height/3));
		userListScroll.setMaximumSize(new Dimension(screenSize.width*3/13,screenSize.height));
		/**userList**/
		
		/**enterMessage**/
						
		enterMessage.setEditable(true);
		
		DefaultCaret enterMessageCaret = (DefaultCaret)enterMessage.getCaret();
		enterMessageCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane enterMessageScroll = new JScrollPane();
		enterMessageScroll.setViewportView(enterMessage);
		
		
		enterMessageScroll.setPreferredSize(new Dimension(screenSize.width*3/(6*10),screenSize.height/20));
		/**enterMessage**/
						
		/**sendButton**/
		
		/**sendButton**/
		
	    Box horizontal1 = Box.createHorizontalBox();
	    Box horizontal2 = Box.createHorizontalBox();
	    Box vertical1 = Box.createVerticalBox();
	    Box vertical2 = Box.createVerticalBox();
	    
	    horizontal1.add(Box.createHorizontalStrut(3));
	    horizontal1.add(mainMessagesScroll);
	    horizontal1.add(Box.createHorizontalStrut(6));
	    horizontal1.add(userListScroll);
	    horizontal1.add(Box.createHorizontalStrut(1));
	    
	    vertical1.add(Box.createVerticalStrut(3));
	    vertical1.add(horizontal1);
	    vertical1.add(Box.createVerticalStrut(6));
	    
	    horizontal2.add(Box.createHorizontalStrut(3));
	    horizontal2.add(enterMessageScroll);
	    horizontal2.add(Box.createHorizontalStrut(6));
	    horizontal2.add(sendButton);    
	    
	    horizontal2.add(Box.createHorizontalStrut(3));
	    
	    vertical2.add(horizontal2);
	    vertical2.add(Box.createVerticalStrut(3));
	    
	    MainChatFrame.add(vertical1);
	    MainChatFrame.add(vertical2,"South");
	    
	    
	    MainChatFrame.setVisible(true); 
	  
	}
	
	public String gettextMessage()
	{
		System.out.println(enterMessage.getText());
		return enterMessage.getText();
	}
	
	public void update(Observable obs) {
		ChatWindowModel model;		
		
		model=(ChatWindowModel)obs;
		
		if(!model.gettextMessage().equals(""))
		{
		mainMessages.append(model.gettextMessage()+"\n");
		model.settextMessage("");
		}
		
		
		userList.setText(null);
		
		for(String str:model.getusersInChat())
		userList.append(str+"\n");		
	}
	
	
	public void addController(ActionListener controller){
		
		sendButton.addActionListener(controller); 
		sendButton.addKeyListener((KeyListener) controller);
		enterMessage.addKeyListener((KeyListener) controller);
		MainChatFrame.addWindowListener((WindowListener) controller);
		
		
	} //addController()

	public JButton getSendButton() {
		return sendButton;
	}

	public void setSendButton(JButton sendButton) {
		this.sendButton = sendButton;
	}

	public JTextArea getEnterMessage() {
		return enterMessage;
	}

	public void setEnterMessage(JTextArea enterMessage) {
		this.enterMessage = enterMessage;
	}
	
	
	
}
