package ImagePanels;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {
    private Image image;
    
    public Image getImage() {
        return image;
    }
    public void setImageFile(String imageLocation) {
    	Image image;
    	try {
			image=ImageIO.read(new File(imageLocation));
			this.image = image;
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(image != null){
            g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        }
    }
}