package Networking;

import java.net.*;
import java.io.*;

public class Client {

	private InetAddress ipServerAdress;
	private int serverPort;
	private Socket socket;
	private OutputStream sout; 
	private DataOutputStream out;
	private InputStream sin; 
	private DataInputStream in;
	private boolean isSuchNameRegisteredOnServer;
	private String clientName;
	
	public void setclientName(String name)
	{
		this.clientName=name;
	}
	
	public String getclientName()
	{
		return clientName;
	}
	
	public boolean connectToServer(String portAdress, String ipAdress) {
		try {
			ipServerAdress = InetAddress.getByName(ipAdress);
			serverPort = Integer.parseInt(portAdress);
			socket = new Socket(ipServerAdress, serverPort);
			
			sout = socket.getOutputStream();
			out = new DataOutputStream(sout);
			
			sin = socket.getInputStream();
			in = new DataInputStream(sin);
			
			
			return true;
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}
    public boolean registerOnServer(String registerName)
    {
    	try {
			System.out.println("Client: try register new user on server");
    		
    		out.writeUTF(registerName);
			isSuchNameRegisteredOnServer=in.readBoolean();
			
			if(isSuchNameRegisteredOnServer==false)
				System.out.println("Client: registration new user on server done!");
			
			return !isSuchNameRegisteredOnServer;
			
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
    }
	
	public void disconnectFromServer()
	{
		try {
			System.out.println("Client: disconnect from server");
			sendMessage("USER_EXIT");
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void sendMessage(String message)
	{
		try {
			out.writeUTF(message);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String reciveMessage()
	{
		try {
			return in.readUTF();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
