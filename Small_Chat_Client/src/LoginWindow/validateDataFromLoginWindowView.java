package LoginWindow;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import validateDataFromGUI.IValidateDataFromGUI;

public class validateDataFromLoginWindowView implements IValidateDataFromGUI
{
	/**
	 * 
	 * @param container - containerwith some text fields
	 * @return true if any data is in all text areas components, return false in other cases
	 */
	public boolean isAnyDataInAllTextAreasInComponent(Container container)
	{
		Component componentsOfContainer[];
		componentsOfContainer=container.getComponents();		
		
		for(int i=0;i<componentsOfContainer.length;i++)
		{
			if(componentsOfContainer[i].getClass()==JTextField.class)
			{
				JTextField tempTextField;
				tempTextField=(JTextField)componentsOfContainer[i];
				
				if (tempTextField.getText().equals(""))
				{
					return false;
				}
			}
		}
		return true;
	}
}