package LoginWindow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.text.AttributeSet;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

import ChatWindow.ChatWindowModel;
import ImagePanels.ImagePanel;
import Observer.IObserver;
import Observer.Observable;
import WorkWithTextFields.SDocumentTextLimit;

public class LoginWindowView extends JFrame implements IObserver {

	private JFrame loginWindowFrame = new JFrame("Login to Small Chat");

	private JTextField nickName = new JTextField(new SDocumentTextLimit(20),
			"", 15);
	private JLabel nickNameLabel = new JLabel("Nick name");

	private JTextField port = new JTextField(new SDocumentTextLimit(5), "1025", 5);
	private JLabel portLabel = new JLabel(
			"Server port (Is in range from 1025 to 65535)");

	private JTextField serverAdress = new JTextField(20);
	private JLabel serverAdressLabel = new JLabel("Server adress");

	private ImagePanel mainLogo = new ImagePanel();

	private JButton loginButton = new JButton("Login");

	private ImagePanel logginButtonsGround = new ImagePanel();

	private Box enteringDataBox = Box.createVerticalBox();
	private Box enteringButtonsBox = Box.createHorizontalBox();
	private Box allInThisBox = Box.createVerticalBox();

	public LoginWindowView() {

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		serverAdress.setText("127.0.0.1");//standart server adress
		
		/** loginWindowFrame **/

		loginWindowFrame.setSize(new Dimension(screenSize.width / 4,
				screenSize.height / 2));
		
		
		
		loginWindowFrame.setResizable(false);
		loginWindowFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);

		/** loginWindowFrame **/

		nickName.setToolTipText("Enter your nickname here");
		port.setToolTipText("Enter port of the server (1025-65535)");
		serverAdress.setToolTipText("Enter IP adress of the server");
		/***/

		mainLogo.setLayout(new java.awt.BorderLayout());
		
		
		
		mainLogo.setImageFile("mainLogo.bmp");
		/***/

		logginButtonsGround.setLayout(new java.awt.BorderLayout());
		logginButtonsGround.setImageFile("WhiteBlank.bmp");

		enteringDataBox.add(nickNameLabel);
		enteringDataBox.add(nickName);

		enteringDataBox.add(portLabel);
		enteringDataBox.add(port);

		enteringDataBox.add(serverAdressLabel);
		enteringDataBox.add(serverAdress);
		enteringDataBox.add(Box.createVerticalStrut(6));

		enteringButtonsBox.add(Box.createVerticalStrut(0));
		enteringButtonsBox.add(loginButton);
		enteringButtonsBox.add(Box.createHorizontalStrut(6));

		allInThisBox.add(enteringDataBox);
		allInThisBox.add(enteringButtonsBox);
		allInThisBox.add(Box.createVerticalStrut(6));

		logginButtonsGround.add(allInThisBox);

		loginWindowFrame.add(mainLogo);

		System.out.println(loginWindowFrame.getSize().toString());
		
		
		loginWindowFrame.add(logginButtonsGround, BorderLayout.SOUTH);

		loginWindowFrame.setLocationRelativeTo(null);
		loginWindowFrame.setVisible(true);
	}

	public void update(Observable obs) {
		LoginWindowModel model;
		model = (LoginWindowModel) obs;
	}

	public void addController(ActionListener controller) {
		System.out.println("LoginWindowView      : adding controller");
		loginButton.addActionListener((ActionListener) controller);
		loginButton.addKeyListener((KeyListener) controller);
		nickName.addKeyListener((KeyListener) controller);
		port.addKeyListener((KeyListener) controller);
		serverAdress.addKeyListener((KeyListener) controller);

	}

	/*** DIALOGS ***/
	public void messageDialogSuchUsernameHasAlredyRegistered() {
		JOptionPane.showMessageDialog(loginWindowFrame,
				"Registration problem: such nickname has alredy registered", "Registration problem",
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void messageDialogEnterDataInAllFields() {
		JOptionPane.showMessageDialog(loginWindowFrame,
				"Entering data problem: enter data in all fields", "Entering data problem",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void messageDialogConnectionProblem() {
		JOptionPane.showMessageDialog(loginWindowFrame,
				"Connection problem: try enter another IP and/or Port",
				"Conection problem", JOptionPane.INFORMATION_MESSAGE);
	}

	/*** DIALOGS ***/

	/*** GETTERS ***/
	public JTextField getNickName() {
		return nickName;
	}

	public JTextField getPort() {
		return port;
	}

	public JTextField getServerAdress() {
		return serverAdress;
	}

	public JButton getLoginButton() {
		return loginButton;
	}

	public JFrame getLoginWindowFrame() {
		return loginWindowFrame;
	}

	public Box getEnteringDataBox() {
		return enteringDataBox;
	}

	/*** GETTERS ***/

	/*** SETTERS ***/
	public void setNickName(JTextField nickName) {
		this.nickName = nickName;
	}

	public void setPort(JTextField port) {
		this.port = port;
	}

	public void setServerAdress(JTextField serverAdress) {
		this.serverAdress = serverAdress;
	}

	public void setLoginButton(JButton loginButton) {
		this.loginButton = loginButton;
	}

	public void setLoginWindowFrame(JFrame loginWindowFrame) {
		this.loginWindowFrame = loginWindowFrame;
	}

	public void setEnteringDataBox(Box enteringDataBox) {
		this.enteringDataBox = enteringDataBox;
	}
	/*** SETTERS ***/

}