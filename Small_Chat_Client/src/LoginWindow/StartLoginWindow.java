package LoginWindow;

public class StartLoginWindow {

	public StartLoginWindow()
	{
		/***LOGINWINDOWMODEL***/
		LoginWindowModel loginWindowModel=new LoginWindowModel();
		LoginWindowView loginWindowView=new LoginWindowView();
		
		loginWindowModel.addObserver(loginWindowView);
				
		LoginWindowController loginWindowController=new LoginWindowController();
		
		loginWindowController.setModel(loginWindowModel);
		loginWindowController.setView(loginWindowView);
		
		loginWindowView.addController(loginWindowController);
		/***LOGINWINDOWMODEL***/
	}
	
}
