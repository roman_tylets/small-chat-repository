package LoginWindow;

import Observer.Observable;

public class LoginWindowModel extends Observable{
	
	private String nickName = new String();
	private String port = new String();
	private String serverAdress = new String();
	
	public LoginWindowModel(){
	
	}
	
	public String getnickName()
	{
		return nickName;
	}
	
	public void setnickName(String nickName)
	{
		if (nickName == null)
			throw new NullPointerException("Null parametr");
		this.nickName = nickName;
		notifyObservers();
	}
	
	public String getport()
	{
		return port;
	}
	
	public void setport(String port)
	{
		if (port == null)
			throw new NullPointerException("Null parametr");
		this.port = port;
		notifyObservers();
	}
	
	public String getserverAdress()
	{
		return serverAdress;
	}
	
	public void setserverAdress(String serverAdress)
	{
		if (serverAdress == null)
			throw new NullPointerException("Null parametr");
		this.serverAdress = serverAdress;
		notifyObservers();
	}
	
}
