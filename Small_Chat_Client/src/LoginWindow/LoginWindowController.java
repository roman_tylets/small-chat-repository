package LoginWindow;

import java.awt.Container;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import ChatWindow.StartChatWindow;
import MVC.Controller.IController;
import Networking.Client;
import Observer.IObserver;
import Observer.Observable;

public class LoginWindowController extends validateDataFromLoginWindowView implements IController, ActionListener, KeyListener{

	private LoginWindowModel model;
	private LoginWindowView view;
	private Client client = new Client();
	
	public LoginWindowController()
	{
	}
	
	/**Listeners of View**/
	
	/***KEY LISTENER**/
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent arg0) {
		
		
	}
	
	public void keyPressed(KeyEvent e) { 
		
		if(e.getSource()==view.getNickName())
		{
			doRegistration(e);
		}
		else if(e.getSource()==view.getPort())
		{
			doRegistration(e);
		}
		else if(e.getSource()==view.getServerAdress())
		{
			doRegistration(e);
		}
		else if(e.getSource()==view.getLoginButton())
		{
			doRegistration(e);
		}
		else
		{
			System.err.println("LoginWindowController KeyEvent: unknown command");
		}
		
	}
	/***KEY LISTENER**/
	
	
	/***ACTION LISTENER**/
	public void actionPerformed(ActionEvent e){
		
		if(e.getSource()==view.getLoginButton())
		{
			doRegistration();
			
		}
		
		else
		{
			System.err.println("LoginWindowController ActionEvent: unknown command");
		}
		
				
	} 
	/***ACTION LISTENER**/
	/**Listeners of View**/
	
	public void setModel(Observable model)
	{
		this.model=(LoginWindowModel)model;
	}
	
	public void setView(IObserver view)
	{
		this.view=(LoginWindowView)view;
	}

	/**
	 * function nonoDataInSomeTextField() calls messageDialogEnterDataInAllFields() if no data in any text field
	 * @return true if in some text field of login window no data, return false if is all text fields are some data
	 */
	
	public boolean noDataInSomeTextField() 
	{
		
            if(!isAnyDataInAllTextAreasInComponent(view.getEnteringDataBox()))
            {
            view.messageDialogEnterDataInAllFields();
            return true;
            }
            
           return false;
	}
	
	
	/**
	 * function doRegistration(KeyEvent e) - register client on a server, if connection fail call messageDialogConnectionProblem(), if connection done - calls main chat window;
	 * @param e - KeyEvent
	 */
	public void doRegistration(KeyEvent e)
	{
			
		if(e.getKeyCode() == KeyEvent.VK_ENTER)
		{
			doRegistration();				
		}
		
		
	}
	
	
	public boolean noConnectionToServer()
	{
		if(!client.connectToServer(view.getPort().getText(), view.getServerAdress().getText()))
		{
			view.messageDialogConnectionProblem();
			return true;
		}
		return false;
	}
	
	
	public boolean noRegistredToServer()
	{
		
		if(client.registerOnServer(view.getNickName().getText())==false)
		{
		client.disconnectFromServer();
		view.messageDialogSuchUsernameHasAlredyRegistered();
		return true;
		}
		client.setclientName(view.getNickName().getText());
		return false;
	}
	
	
	
	/**
	 * function doRegistration() - register client on a server, if connection fail call messageDialogConnectionProblem(), if connection done - calls main chat window;
	 * @param e - KeyEvent
	 */
	public void doRegistration()
	{
		
		
		
		
		
		if(!noDataInSomeTextField())
		{
				
			if(!noConnectionToServer())

			{
							
				if(!noRegistredToServer())	
			
				{	
					view.getLoginWindowFrame().setVisible(false);
					StartChatWindow startChatWindow = new StartChatWindow(client);//Starts Chat Window After COmplete Registration
				}
				
			}
		}
		

	}
		
	

}
