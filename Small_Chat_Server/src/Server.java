import java.net.*;

import java.util.ArrayList;

import java.util.List;

import java.io.*;


interface IClientData
{
} 

class SmallChatClientData  implements IClientData
{

	private List<String> clientNames;
	private String clientMessage;
	
	SmallChatClientData()
	{
		clientNames=new ArrayList<String>();
	}
	
	
	public void addClientName(String clientName) {
		clientNames.add(clientName);
	}

	public void removeClientName(String clientName) {
		clientNames.remove(clientName);
	}
	
	
	public void setClientMessage(String clientMessage) {
		this.clientMessage=clientMessage;
	}

	
	public List<String> getClientNames() {
		return clientNames;
	}

	public String getClientMessage() {
		return clientMessage;
	}
}

interface IServerData
{
}

class SmallChatServerData implements IServerData
{
	private List<Socket> clientSockets;
	
	SmallChatServerData()
	{
		clientSockets=new ArrayList<Socket>();
	}
	
	public void addClientSoket (Socket clientSocket)
	{
		clientSockets.add(clientSocket);
	}
	
	public void removeClientSoket (Socket clientSocket)
	{
		clientSockets.remove(clientSocket);
	}
	
	public List<Socket> getClientSockets()
	{
		return clientSockets;
	}
	
	
}



public class Server {
	
	private static final int  SERVER_PORT = 1025;
	private SmallChatClientData clientData; 
	private SmallChatServerData serverData;
	private boolean isSuchNameRegisteredOnServer;
	private Socket clientSocket;
	private ServerSocket serverSocket;
	
	Server()
	{
		clientData = new SmallChatClientData();
		serverData = new SmallChatServerData();
		startServer();
	}
	
	public void startServer()
	{
       try{
    	   serverSocket=null;
    	   clientSocket=null;
		try {
         serverSocket = new ServerSocket(SERVER_PORT); 
         
         while(true)
         {
         System.out.println("Waiting for a client...");
         clientSocket = serverSocket.accept(); 
         System.out.println("Got a client :) ... Finally, someone saw me through all the cover!");
         SmallChatServerThread serverThread = new SmallChatServerThread(clientSocket);
         serverThread.start();
       } 
		}
		finally{
			 System.out.println("Closing server...");
			 serverSocket.close();
		}
         
      } catch(IOException e) { e.printStackTrace(); }
   }
	
class SmallChatServerThread extends Thread
{
	private Socket clientSocket;
	
	public SmallChatServerThread(Socket socket)
	{
		this.clientSocket=socket;
	}
	
	@Override
	public void run()
	{
		String currentMessage=null;
		DataOutputStream outtmp=null;
		String userName=null;
		try{
			
		InputStream sin = clientSocket.getInputStream();
        OutputStream sout = clientSocket.getOutputStream();

        
        DataInputStream in = new DataInputStream(sin);
        DataOutputStream out = new DataOutputStream(sout);
        
        System.out.println(clientData.getClientNames());
        
		userName = in.readUTF();
		
        isSuchNameRegisteredOnServer=clientData.getClientNames().contains(userName);
        System.out.println("isSuchNameRegisteredOnServer = "+isSuchNameRegisteredOnServer);
        
		out.writeBoolean(isSuchNameRegisteredOnServer);
		
        if(!isSuchNameRegisteredOnServer)
        {
       	 clientData.addClientName(userName);
       	 serverData.addClientSoket(clientSocket);
       	 
       	 
       	 
       	for(Socket tmpClientSocket : serverData.getClientSockets() )
      	 {
       		outtmp = new DataOutputStream(tmpClientSocket.getOutputStream());
       	 for(String str:clientData.getClientNames())
       	 { outtmp.writeUTF("USER_NAME_"+str);
       	 }
       	 }
       	 
       	 
        currentMessage=in.readUTF();
		
        System.out.println("Mesage come: "+currentMessage);
        
        while(!currentMessage.equals("USER_EXIT"))
        {
        	
        	
        	for(Socket tmpClientSocket : serverData.getClientSockets() )
       	 {
       		 
				outtmp = new DataOutputStream(tmpClientSocket.getOutputStream());
				outtmp.writeUTF(userName+": "+currentMessage);
				
                System.out.println("Message send: "+currentMessage);
       	 }
       	currentMessage=in.readUTF();
		System.out.println("Mesage come: "+currentMessage);
		
        }
        
         
        System.out.println("Server: Client quit, name = "+userName);
        clientData.removeClientName(userName); 
        serverData.removeClientSoket(clientSocket);
        
        
        for(Socket tmpClientSocket : serverData.getClientSockets() )
      	 {
      		 
				outtmp = new DataOutputStream(tmpClientSocket.getOutputStream());
				
				outtmp.writeUTF("QUIT_USER_NAME_"+userName);
      	 
      	 }
        
        
        clientSocket.close();
		
        } 
        
        }catch (IOException e) {
			e.printStackTrace();
		}
        		
	}
	
}

}





